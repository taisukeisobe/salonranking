
require 'nokogiri'
require 'open-uri'
require 'csv'

title = nil
num = nil

for i in 1..100 do

  url = 'https://camp-fire.jp/channels/fanclub/page:' + i.to_s

  charset = nil
  begin
    html = open(url)
  rescue StandardError
    puts '404'
    next
  end


  html = open(url) do |f|
    charset = f.charset
    f.read

    doc = Nokogiri::HTML.parse(html, nil, charset)
    box = doc.css('.card')


    box.each do |node|
      page = 'https://camp-fire.jp' + node.css('.card__inner__text__title > a')[0][:href]
      description = node.css('.card__inner__text__description').text
      title = node.css('.card__inner__text__title').text
      member = node.css('.card__inner__status > ul > li').text


      CSV.open('campfire3.csv', 'a') do |test|
        test << [title.strip,member.strip.chop,page.strip,description.strip]

      end
    end
  end

  end
